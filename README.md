# clock_app

Stockbit - Test Assignment Flutter Engineer.

Download :

- git url : git clone https://gitlab.com/iqballl/clock_app.git
- Open a text editor (Visual Studio Code or Android Studio).
- Open the project folder that has been successfully downloaded or cloned.
- Once the flutter project is open, don't forget to install the packages used. You do this by opening the pubspec.yaml file and clicking Get Packages on the VS Code option.
- After that, the packages will be installed and the project is ready to run.
- You are done with installing the packages, the errors are corrected and you can start your project, or check the code.

## Another way to do the installation

Following are the steps to follow :
- Open VScode and take a new window.
- Press " ctrl+shift+p " for making command pallet to display.
- Type in Git in the pallet on top.
- Select the suggested Git:clone option.
- And lastly, paste the Git URL in the pallet of the project you have cloned.


## Running and debugging
- Start debugging by clicking Run > Start Debugging from the main IDE window, or press F5.

import 'package:clock_app/constants/size_config.dart';
import 'package:clock_app/views/widgets/digital_clock.dart';
import 'package:flutter/material.dart';

import 'alarm_page.dart';
import 'widgets/clock_view.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // we have to call this on our starting page
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: const Color(0xFF2D2F41),
      body: SafeArea(
        child: Column(
          children: [
            DigitalClockWidget(),
            SizedBox(
              height: 20,
            ),
            Align(
              alignment: Alignment.center,
              child: ClockView(
                size: MediaQuery.of(context).size.height / 4,
              ),
            ),
            Flexible(child: AlarmPage())
          ],
        ),
      ),
    );
  }
}

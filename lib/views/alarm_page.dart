import 'package:clock_app/database_helper.dart';
import 'package:clock_app/constants/theme_data.dart';
import 'package:clock_app/main.dart';
import 'package:clock_app/models/alarm_info.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:intl/intl.dart';

import 'package:timezone/data/latest_all.dart' as tz;
import 'package:timezone/timezone.dart' as tz;

class AlarmPage extends StatefulWidget {
  AlarmPage({Key? key}) : super(key: key);

  @override
  _AlarmPageState createState() => _AlarmPageState();
}

class _AlarmPageState extends State<AlarmPage> {
  late DateTime _alarmTime;
  late String _alarmTimeString;
  Future<List<AlarmInfo>> _alarms = Future.value([]);
  List<AlarmInfo> _currentAlarms = <AlarmInfo>[];

  @override
  void initState() {
    _alarmTime = DateTime.now();
    DatabaseHelper.instance.initializeDatabase().then((value) {
      print(' -- Database intialized -- ');
      loadAlarms();
    });
    super.initState();
  }

  void loadAlarms() {
    _alarms = DatabaseHelper.instance.getAlarms();
    if (mounted) setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 28, right: 28),
            child: Text(
              'Alarm List',
              style: TextStyle(
                fontFamily: 'avenir',
                color: Colors.white,
                fontWeight: FontWeight.w600,
                fontSize: 18,
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(
              left: 25.0,
              right: 25.0,
            ),
            child: Divider(
              color: CustomColors.clockOutline,
            ),
          ),
          Expanded(
            child: FutureBuilder(
              future: _alarms,
              builder: (BuildContext context,
                  AsyncSnapshot<List<AlarmInfo>> snapshot) {
                if (snapshot.hasData) {
                  _currentAlarms = snapshot.data!;
                  return ListView(
                    padding: EdgeInsets.only(
                      left: 28,
                      right: 28,
                      top: 8.0,
                      bottom: 8.0,
                    ),
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    children: snapshot.data!.map<Widget>((alarmData) {
                      var alarmDate = DateFormat('EEE, d MMM')
                          .format(alarmData.alarmDateTime);
                      var alarmTime = DateFormat('hh:mm aa')
                          .format(alarmData.alarmDateTime);
                      var gradientColor = GradientTemplate
                          .gradientTemplate[alarmData.gradientColorIndex]
                          .colors;
                      return Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                        margin: EdgeInsets.only(bottom: 22),
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: gradientColor,
                              begin: Alignment.centerLeft,
                              end: Alignment.centerRight,
                            ),
                            borderRadius: BorderRadius.all(
                              Radius.circular(24),
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.red.withOpacity(0.4),
                                blurRadius: 8,
                                spreadRadius: 4,
                                offset: Offset(4, 4),
                              )
                            ]),
                        child: Container(
                          padding: EdgeInsets.all(5.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.label,
                                        color: Colors.white,
                                        size: 24,
                                      ),
                                      SizedBox(
                                        width: 8,
                                      ),
                                      Text(
                                        alarmData.title,
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: 'avenir',
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                alarmDate,
                                style: TextStyle(
                                  fontFamily: 'avenir',
                                  color: Colors.white,
                                ),
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    alarmTime,
                                    style: TextStyle(
                                        fontFamily: 'avenir',
                                        color: Colors.white,
                                        fontSize: 24,
                                        fontWeight: FontWeight.w700),
                                  ),
                                  IconButton(
                                    icon: Icon(Icons.delete),
                                    color: Colors.white,
                                    onPressed: () {
                                      deleteAlarm(alarmData.id);
                                    },
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      );
                    }).followedBy([
                      if (_currentAlarms.length < 5)
                        DottedBorder(
                          strokeWidth: 2,
                          color: CustomColors.clockOutline,
                          borderType: BorderType.RRect,
                          radius: Radius.circular(24),
                          dashPattern: [5, 4],
                          child: Container(
                            width: double.infinity,
                            decoration: BoxDecoration(
                              color: CustomColors.clockBG,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(24)),
                            ),
                            child: FlatButton(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 32, vertical: 16),
                              onPressed: () {
                                _alarmTimeString =
                                    DateFormat('HH:mm').format(DateTime.now());
                                showModalBottomSheet(
                                  useRootNavigator: true,
                                  context: context,
                                  clipBehavior: Clip.antiAlias,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.vertical(
                                      top: Radius.circular(24),
                                    ),
                                  ),
                                  builder: (context) {
                                    return StatefulBuilder(
                                      builder: (context, setModalState) {
                                        return Container(
                                          height: MediaQuery.of(context)
                                                  .copyWith()
                                                  .size
                                                  .height *
                                              0.30,
                                          padding: const EdgeInsets.all(32),
                                          child: Column(
                                            children: [
                                              FlatButton(
                                                onPressed: () async {
                                                  var selectedTime =
                                                      await showTimePicker(
                                                    context: context,
                                                    initialTime:
                                                        TimeOfDay.now(),
                                                  );
                                                  if (selectedTime != null) {
                                                    final now = DateTime.now();
                                                    var selectedDateTime =
                                                        DateTime(
                                                            now.year,
                                                            now.month,
                                                            now.day,
                                                            selectedTime.hour,
                                                            selectedTime
                                                                .minute);
                                                    _alarmTime =
                                                        selectedDateTime;
                                                    setModalState(() {
                                                      _alarmTimeString =
                                                          DateFormat('HH:mm')
                                                              .format(
                                                                  selectedDateTime);
                                                    });
                                                  }
                                                },
                                                child: Text(
                                                  _alarmTimeString,
                                                  style:
                                                      TextStyle(fontSize: 32),
                                                ),
                                              ),
                                              Text(
                                                'Click time to change',
                                                style: TextStyle(
                                                  fontSize: 12,
                                                  fontStyle: FontStyle.italic,
                                                ),
                                              ),
                                              SizedBox(
                                                height: 20,
                                              ),
                                              FloatingActionButton.extended(
                                                onPressed: onSaveAlarm,
                                                icon: Icon(Icons.alarm),
                                                label: Text('Save'),
                                              ),
                                            ],
                                          ),
                                        );
                                      },
                                    );
                                  },
                                );
                                // scheduleAlarm();
                              },
                              child: Column(
                                children: <Widget>[
                                  Image.asset(
                                    'assets/add_alarm.png',
                                    scale: 1.5,
                                  ),
                                  SizedBox(height: 8),
                                  Text(
                                    'Add Alarm',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: 'avenir'),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        )
                      else
                        Center(
                          child: Text(
                            'Only 5 alarms allowed!',
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                    ]).toList(),
                  );
                } else {
                  return Center(child: Text('Loading...'));
                }
              },
            ),
          )
        ],
      ),
    );
  }

  void scheduleAlarm(DateTime scheduledNotificationDateTime,
      AlarmInfo alarmInfo, int insertedId) async {
    tz.initializeTimeZones();
    // var scheduledNotificationDateTime =
    //     DateTime.now().add(Duration(seconds: 10));

    final sound = 'mixkit_happy_bells.wav';
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      '$insertedId', // channel ID
      'channel_name_$insertedId', // channel name
      channelDescription: 'Channel for Alarm notification',
      icon: 'icon_notification',
      sound: RawResourceAndroidNotificationSound(sound.split('.').first),
      importance: Importance.high,
      priority: Priority.high,
      playSound: true,
      enableVibration: true,
    );

    var iOSPlatformChannelSpecifics = IOSNotificationDetails(
        sound: sound,
        presentAlert: true,
        presentBadge: true,
        presentSound: true);
    var platformChannelSpecifics = NotificationDetails(
      android: androidPlatformChannelSpecifics,
      iOS: iOSPlatformChannelSpecifics,
    );

    await flutterLocalNotificationsPlugin.zonedSchedule(
        insertedId,
        'Office',
        alarmInfo.title,
        tz.TZDateTime.from(scheduledNotificationDateTime, tz.local),
        platformChannelSpecifics,
        androidAllowWhileIdle: true,
        uiLocalNotificationDateInterpretation:
            UILocalNotificationDateInterpretation.absoluteTime);
  }

  Future onSaveAlarm() async {
    DateTime scheduleAlarmDateTime;
    if (_alarmTime.isAfter(DateTime.now())) {
      scheduleAlarmDateTime = _alarmTime;
    } else {
      scheduleAlarmDateTime = _alarmTime.add(Duration(days: 1));
    }

    var alarmInfo = AlarmInfo(
        title: 'Alarm notification',
        alarmDateTime: scheduleAlarmDateTime,
        gradientColorIndex: _currentAlarms.length);

    final insertedId = await DatabaseHelper.instance.insertAlarm(alarmInfo);
    print('result : $insertedId');
    scheduleAlarm(scheduleAlarmDateTime, alarmInfo, insertedId);
    Navigator.pop(context);
    loadAlarms();
  }

  void deleteAlarm(int? id) {
    DatabaseHelper.instance.delete(id);
    //unsubscribe for notification
    loadAlarms();
  }
}

import 'package:clock_app/models/alarm_info.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:path/path.dart';

final String dbname = "alarm.db";
final int dbversion = 1;
final String tableAlarm = 'alarm';
final String columnId = 'id';
final String columnTitle = 'title';
final String columnDateTime = 'alarmDateTime';
final String columnPending = 'isPending';
final String columnColorIndex = 'gradientColorIndex';

class DatabaseHelper {
  // make this a singleton class
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  // only have a single app-wide reference to the database
  static Database? _database;
  Future<Database?> get database async {
    // other option
    // if (_database == null) {
    //    _database ??= await initializeDatabase();
    // }
    if (_database != null) {
      return _database;
    }
    _database = await initializeDatabase();
    return _database;
  }

  // this opens the database (and creates it if it doesn't exist)
  initializeDatabase() async {
    var dir = await getDatabasesPath();
    var path = join(dir, dbname);

    // var database = await openDatabase(
    //   path,
    //   version: 1,
    //   onCreate: (db, version) {
    //     db.execute('''
    //       create table $tableAlarm (
    //       $columnId integer primary key autoincrement,
    //       $columnTitle text not null,
    //       $columnDateTime text not null,
    //       $columnPending integer,
    //       $columnColorIndex integer)
    //     ''');
    //   },
    // );
    // return database;
    return await openDatabase(path, version: dbversion, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int dbversion) async {
    return await db.execute('''
          create table $tableAlarm ( 
          $columnId integer primary key autoincrement, 
          $columnTitle text not null,
          $columnDateTime text not null,
          $columnPending integer,
          $columnColorIndex integer)
        ''');
  }

  Future close() async {
    final db = await instance.database;
    db!.close();
  }

  Future<int> insertAlarm(AlarmInfo alarmInfo) async {
    var db = await instance.database;
    return await db!.insert(tableAlarm, alarmInfo.toMap());
  }

  Future<List<AlarmInfo>> getAlarms() async {
    List<AlarmInfo> _alarms = [];

    var db = await instance.database;
    var result = await db!.query(tableAlarm);
    result.forEach((element) {
      var alarmInfo = AlarmInfo.fromMap(element);
      _alarms.add(alarmInfo);
    });

    return _alarms;
  }

  Future<int> delete(int? id) async {
    var db = await instance.database;
    return await db!
        .delete(tableAlarm, where: '$columnId = ?', whereArgs: [id]);
  }

  // void insertAlarm(AlarmInfo alarmInfo) async {
  //   var db = await instance.database;
  //   var result = await db!.insert(tableAlarm, alarmInfo.toMap());
  //   print('result : $result');
  //   //return result;
  // }

  // Future<List<AlarmInfo>> getAlarms() async {
  //   var db = await instance.database;
  //   var result = await db!.query(tableAlarm);
  //   List<AlarmInfo> _alarms = result.isNotEmpty
  //       ? result.map((c) => AlarmInfo.fromMap(c)).toList()
  //       : [];
  //   return _alarms;
  // }

}
